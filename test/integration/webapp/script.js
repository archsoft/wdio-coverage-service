window.test = function () {
  let message = ''
  if (Math.random() >= 0.5) {
    message = 'a'
  } else {
    message = 'b'
  }
  var node = document.createElement('LI') // Create a <li> node
  var textnode = document.createTextNode(message) // Create a text node
  node.appendChild(textnode) // Append the text to <li>
  document.getElementById('log').appendChild(node)
}
